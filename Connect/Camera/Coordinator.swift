//
//  Coordinator.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-09-18.
//

import SwiftUI
import AVFoundation

class Coordinator: NSObject, UINavigationControllerDelegate, AVCapturePhotoCaptureDelegate {
    
    let parent: CustomCameraRepresentable
    
    init(_ parent: CustomCameraRepresentable) {
        self.parent = parent
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        parent.didTapCapture = false
        
        if let imageData = photo.fileDataRepresentation() {
            parent.image = UIImage(data: imageData)
            parent.didSaveImage = true
        }
    }
}
