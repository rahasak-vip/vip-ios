//
//  CustomCameraRepresentable.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-09-18.
//

import SwiftUI
import AVFoundation

struct CustomCameraRepresentable: UIViewControllerRepresentable {
    
    @Binding var image: UIImage?
    @Binding var didTapCapture: Bool
    @Binding var didSaveImage: Bool
    
    func makeUIViewController(context: Context) -> CustomCameraController {
        let controller = CustomCameraController()
        controller.delegate = context.coordinator
        return controller
    }
    
    func updateUIViewController(_ cameraViewController: CustomCameraController, context: Context) {
        if(self.didTapCapture) {
            cameraViewController.didTapRecord()
        }
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
}
