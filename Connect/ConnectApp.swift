//
//  ConnectApp.swift
//  Connect
//
//  Created by Arosha Piyadigama on 7/20/21.
//

import SwiftUI

@main
struct ConnectApp: App {
    var body: some Scene {
        WindowGroup {
            LoginView()
        }
    }
}
