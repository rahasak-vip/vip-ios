//
//  CaptureButtonView.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-09-18.
//

import SwiftUI

struct CaptureButtonView: View {
    var body: some View {
        Image(systemName: "camera").font(.largeTitle)
            .frame(width: 70, height: 70)
            .background(Color.green)
            .foregroundColor(.white)
            .clipShape(Circle())
    }
}

struct CaptureButtonView_Previews: PreviewProvider {
    static var previews: some View {
        CaptureButtonView()
    }
}
