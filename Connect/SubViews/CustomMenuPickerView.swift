//
//  CustomMenuPickerView.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-09-19.
//

import SwiftUI

struct CustomMenuPickerView: View {
    
    @Binding var selection: String
    var title: String = "Placeholder"
    var dataArray: [String] = []
    
    var body: some View {
        Picker(selection: $selection,
               label: GreenLineTextView(title: title, text: $selection)) {
            ForEach(dataArray, id: \.self) { result in
                Text(result)
                    .tag(result)
            }
        }
        .pickerStyle(MenuPickerStyle())
    }
}

struct CustomMenuPickerView_Previews: PreviewProvider {
    
    @State static var selection = ""
    
    static var previews: some View {
        CustomMenuPickerView(selection: $selection)
    }
}
