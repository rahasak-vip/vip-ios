//
//  CustomProgressView.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-08-29.
//

import SwiftUI

struct CustomProgressView: View {
    var body: some View {
        ZStack {
            Color(.systemBackground)
                .ignoresSafeArea()
                .opacity(0.5)
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle(tint: Color.green))
                .scaleEffect(2)
        }
    }
}

struct CustomProgressView_Previews: PreviewProvider {
    static var previews: some View {
        CustomProgressView()
    }
}
