//
//  GrayUnderlinedText.swift
//  Connect
//
//  Created by Arosha Piyadigama on 7/22/21.
//

import SwiftUI

struct GrayUnderlinedText: View {
    
    var text:String
    
    var body: some View {
        Text(text)
            .font(.subheadline)
            .foregroundColor(.gray)
            .padding(.bottom, 5)
            .overlay(
                Rectangle()
                    .frame(height: 1)
                    .foregroundColor(Color.gray.opacity(0.2)),
                alignment: .bottom
            )
    }
}

struct GrayUnderlinedText_Previews: PreviewProvider {
    static var previews: some View {
        GrayUnderlinedText(text: "Placeholder")
    }
}
