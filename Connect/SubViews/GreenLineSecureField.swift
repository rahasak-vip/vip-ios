//
//  GreenLineSecureField.swift
//  Connect
//
//  Created by Arosha Piyadigama on 7/20/21.
//

import SwiftUI

struct GreenLineSecureField: View {
    
    var title: String
    @Binding var text: String
    
    var body: some View {
        SecureField(title, text: $text)
            .keyboardType(.default)
            .autocapitalization(.none)
            .disableAutocorrection(true)
            .textFieldStyle(InputGreenLineStyle())
    }
}

struct GreenLineSecureField_Previews: PreviewProvider {
    @State static var text = ""
    
    static var previews: some View {
        GreenLineSecureField(title: "Placeholder", text: $text)
    }
}
