//
//  GreenLineTextView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 7/20/21.
//

import SwiftUI

struct GreenLineTextView: View {
    
    var title: String
    @Binding var text: String
    var keyboardType = UIKeyboardType.default
    var autoCapType = UITextAutocapitalizationType.none
    
    var body: some View {
        TextField(title, text: $text)
            .keyboardType(keyboardType)
            .autocapitalization(autoCapType)
            .disableAutocorrection(true)
            .textFieldStyle(InputGreenLineStyle())
    }
}

struct GreenLineTextView_Previews: PreviewProvider {
    @State static var text = ""
    
    static var previews: some View {
        GreenLineTextView(title: "Placeholder", text: $text, keyboardType: UIKeyboardType.default)
    }
}
