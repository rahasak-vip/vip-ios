//
//  InputGreenLineStyle.swift
//  Connect
//
//  Created by Arosha Piyadigama on 7/23/21.
//

import Foundation
import SwiftUI

struct InputGreenLineStyle: TextFieldStyle {
    
    @Environment(\.colorScheme) var colorScheme
    
    func _body(configuration: TextField<Self._Label>) -> some View {
            configuration
                .padding(.all, 5)
                .background(colorScheme == .dark ? Color.black : Color.white)
                .overlay(
                    Rectangle()
                        .frame(height: 1)
                        .foregroundColor(Color.green.opacity(0.2)),
                    alignment: .bottom
                )
                .padding([.leading, .trailing], 40)
                .padding([.bottom], 20)
        }
}
