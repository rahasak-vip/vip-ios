//
//  PreferencesCellView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-08-05.
//

import SwiftUI

struct PreferencesCellView: View {
    
    var key = "Key"
    var value = "Value"
    
    var body: some View {
        HStack {
            Text(key)  
            Spacer()
            Text(value)
                .foregroundColor(Color.green)
        }
        .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    }
}

struct PreferencesCellView_Previews: PreviewProvider {
    static var previews: some View {
        PreferencesCellView()
    }
}
