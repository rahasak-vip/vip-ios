//
//  QualificationCellView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-08-05.
//

import SwiftUI

struct QualificationCellView: View {
    
    var qualifyHeading: String
    var qualifySubHeading: String
    var qualifyTime: String
    
    var body: some View {
        HStack {
            Image(systemName: "clock.arrow.circlepath")
                .resizable()
                .frame(width: 30.0, height: 30.0)
                .foregroundColor(.yellow)
                .clipShape(Circle())
            
            VStack (alignment: .leading) {
                Text(qualifyHeading)
                    .padding(.vertical, 4)
                    .frame(maxWidth: .infinity, alignment: .leading)
                Text(qualifySubHeading)
                    .font(.subheadline)
                    .foregroundColor(Color.gray)
                    .frame(maxWidth: .infinity, alignment: .leading)
                Text(" Pending ")
                    .foregroundColor(Color.gray)
                    .padding(.vertical, 4)
                    .overlay(
                        RoundedRectangle(cornerRadius: 40)
                            .stroke(Color.yellow.opacity(0.8), lineWidth: 1)
                    )
            }
            .frame(maxWidth: .infinity)
            .edgesIgnoringSafeArea(.all)
            
            Text(qualifyTime)
                .font(.footnote)
                .foregroundColor(Color.gray)
        }
        .frame(maxWidth: .infinity)
    }
}

struct QualificationCellView_Previews: PreviewProvider {
    static var previews: some View {
        QualificationCellView(qualifyHeading: "OL Qualifications", qualifySubHeading: "Ordinary Level Exam", qualifyTime: "2021-09-03")
            
    }
}
