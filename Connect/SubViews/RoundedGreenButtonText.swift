//
//  RoundedGreenButtonText.swift
//  Connect
//
//  Created by Arosha Piyadigama on 7/22/21.
//

import SwiftUI

struct RoundedGreenButtonText: View {
    
    var text: String
    
    var body: some View {
        Text(text)
            .font(.headline)
            .frame(maxWidth: .infinity)
            .padding()
            .foregroundColor(.gray)
            .overlay(
                RoundedRectangle(cornerRadius: 40)
                    .stroke(Color.green.opacity(0.8), lineWidth: 1)
            )
            .padding(.bottom, 10)
    }
}

struct RoundedGreenButtonText_Previews: PreviewProvider {
    static var previews: some View {
        RoundedGreenButtonText(text: "Placeholder")
    }
}
