//
//  TextFontModifier.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-10-01.
//

import SwiftUI

struct TextFontModifier : ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.custom("GeosansLight", size: 20))
//            .scaledToFit()
//            .minimumScaleFactor(0.01)
    }
}
