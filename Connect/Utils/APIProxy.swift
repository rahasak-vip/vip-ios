//
//  APIProxy.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-08-19.
//

import Foundation
import Network

class APIProxy : ObservableObject {
    
    @Published var isConnected = false
    var statusCode = 0
    
    // Works only iOS 12.0+
    func isConnectedToInternet() {
        
        let monitor = NWPathMonitor()
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                DispatchQueue.main.async {
                    self.isConnected = true
                }
            }
            else {
                DispatchQueue.main.async {
                    self.isConnected = false
                }
            }
        }
        
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
    }
    
    func makeRequestHeader(postData:Data, endpointPath:String) -> URLRequest {
        
        var request = URLRequest(url: URL(string: Constants.baseAPIUrlStr + endpointPath)!, timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = postData
    
        return request
    }
    
    func checkDownloadedDataHasErrors(request:URLRequest, completionHandler: @escaping (_ data: Dictionary<String, Any>) -> ()) {
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if error != nil {
                self.statusCode = 1001
                return
            }
            else {
                guard let data = data else {
                    self.statusCode = 1002
                    return
                }
                
//                print("# " + String(data: data, encoding: .utf8)!)
//                print("## \(response.debugDescription)")
//                print("### \(error.debugDescription)")
                 
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: data, options: [])
                    completionHandler(jsonData as! [String: Any])
                }
                catch {
                    self.statusCode = 1003
                }
            }
        }
        .resume()
    }
    
    private func registerData(user:User) -> String {
        
        var userData = [String: Any]()
        userData[Constants.apiKeyId] = String(Int64(Date().timeIntervalSince1970 * 1000))
        userData[Constants.apiKeyExecer] = user.nic + Constants.execerSuffix
        userData[Constants.apiKeyMessageType] = Constants.messageTypeCreate
        userData[Constants.apiKeyDid] = user.nic
        userData[Constants.apiKeyOwner] = Constants.ismOwner
        userData[Constants.apiKeyPassword] = user.passwordShaValue
        userData[Constants.apiKeyRoles] = []
        userData[Constants.apiKeyGroups] = []
        userData[Constants.apiKeyPubkey] = PreferenceUtil.instance.get(key: PreferenceUtil.PUBLIC_KEY)
        userData[Constants.apiKeyName] = user.name
        userData[Constants.apiKeyGender] = user.gender
        userData[Constants.apiKeyAge] = user.age
        userData[Constants.apiKeyDob] = user.dob
        userData[Constants.apiKeyPhone] = user.phoneNumber
        userData[Constants.apiKeyEmail] = user.email
        userData[Constants.apiKeyAddress] = user.address
        userData[Constants.apiKeyProvince] = user.province
        userData[Constants.apiKeyDistrict] = user.district
        userData[Constants.apiKeyLat] = 0.0
        userData[Constants.apiKeyLon] = 0.0
        userData[Constants.apiKeyBlob] = user.blob
        
        return userData.jsonStringRepresentation!
    }
    
    func registerCall(user:User, completionHandler: @escaping (_ done:Bool) -> ()) {
        
        guard let postData = registerData(user:user).data(using: .utf8) else {
            self.statusCode = 1004
            return
        }
        
        let request = makeRequestHeader(postData: postData, endpointPath: Constants.endPointIdentities)
        
        checkDownloadedDataHasErrors(request: request) { returnedDictionary in
            
            if (returnedDictionary["status"] != nil) {
                self.statusCode = returnedDictionary["status"] as? Int ?? 0
                
                if self.statusCode == 201 {
                    PreferenceUtil.instance.put(key: PreferenceUtil.LAST_LOGIN_NIC, value: user.nic)
                }
            }
            
            let returnedToken = returnedDictionary["token"] as? String
            PreferenceUtil.instance.put(key: PreferenceUtil.LOGIN_TOKEN, value: returnedToken ?? "")
            
            completionHandler(true)
        }
    }
    
    private func connectData(user:User) -> String {
        
        var userData = [String: Any]()
        userData[Constants.apiKeyId] = String(Int64(Date().timeIntervalSince1970 * 1000))
        userData[Constants.apiKeyExecer] = user.nic + Constants.execerSuffix
        userData[Constants.apiKeyMessageType] = Constants.messageTypeConnect
        userData[Constants.apiKeyDid] = user.nic
        userData[Constants.apiKeyOwner] = Constants.ismOwner
        userData[Constants.apiKeyPassword] = user.passwordShaValue
        return userData.jsonStringRepresentation!
    }
    
    func connectCall(user:User, completionHandler: @escaping (_ done:Bool) -> ()) {

        guard let postData = connectData(user:user).data(using: .utf8) else {
            self.statusCode = 1004
            return
        }
        
        let request = makeRequestHeader(postData: postData, endpointPath: Constants.endPointIdentities)
        
        checkDownloadedDataHasErrors(request: request) { returnedDictionary in
            if (returnedDictionary["status"] != nil) {
                self.statusCode = returnedDictionary["status"] as? Int ?? 0
                
                if self.statusCode == 200 || self.statusCode == 405 {
                    PreferenceUtil.instance.put(key: PreferenceUtil.LAST_LOGIN_NIC, value: user.nic)
                }
            }
            else if (returnedDictionary["code"] != nil) {
                self.statusCode = returnedDictionary["code"] as? Int ?? 0
            }
            
            let returnedToken = returnedDictionary["token"] as? String
            PreferenceUtil.instance.put(key: PreferenceUtil.LOGIN_TOKEN, value: returnedToken ?? "")
//            print("# connectCall - returnedDictionary = \(String(describing: returnedDictionary))")
            completionHandler(true)
        }
    }
    
    private func userData(user:User) -> String {
        
        var userData = [String: Any]()
        userData[Constants.apiKeyId] = String(Int64(Date().timeIntervalSince1970 * 1000))
        userData[Constants.apiKeyExecer] = user.nic + Constants.execerSuffix
        userData[Constants.apiKeyMessageType] = Constants.messageTypeGet
        userData[Constants.apiKeyDid] = user.nic
        userData[Constants.apiKeyOwner] = Constants.ismOwner
        
        return userData.jsonStringRepresentation!
    }
    
    func getUserData(user:User, completionHandler: @escaping (_ done:Bool, _ returnedUser:User) -> ()) {
        
        guard let postData = userData(user:user).data(using: .utf8) else {
            self.statusCode = 1004
            return
        }
        var request = makeRequestHeader(postData: postData, endpointPath: Constants.endPointIdentities)
        let loginToken = PreferenceUtil.instance.get(key: PreferenceUtil.LOGIN_TOKEN)
        request.addValue("Bearer \(loginToken)", forHTTPHeaderField: "Authorization")
        
        checkDownloadedDataHasErrors(request: request) { returnedDictionary in
            
            self.statusCode = 200
            
            let loggedInUser = User()
            
            if (returnedDictionary["nic"] != nil) {
                loggedInUser.nic = returnedDictionary["nic"] as! String
            }
            else {
                loggedInUser.nic = returnedDictionary["did"] as! String
            }
            loggedInUser.name = returnedDictionary["name"] as! String
            loggedInUser.dob = returnedDictionary["dob"] as! String
            loggedInUser.phoneNumber = returnedDictionary["phone"] as! String
            loggedInUser.email = returnedDictionary["email"] as! String
            loggedInUser.blob = returnedDictionary["blob"] as! String
            loggedInUser.activated = returnedDictionary["activated"] as! Bool
            
            completionHandler(true, loggedInUser)
        }
    }
    
    private func qualificationData(user: User) -> String {
        
        var userData = [String: Any]()
        userData[Constants.apiKeyId] = String(Int64(Date().timeIntervalSince1970 * 1000))
        userData[Constants.apiKeyExecer] = user.nic + Constants.execerSuffix
        userData[Constants.apiKeyMessageType] = Constants.messageTypeGetQualifications
        userData[Constants.apiKeyQualificationHolder] = user.nic
        
        guard let payload = CryptoUtil.instance.preparePayload(args: userData) else {
            self.statusCode = 1005
            return ""
        }
        let digsig = CryptoUtil.instance.sign(payload: payload)
        userData[Constants.apiKeyMsgSig] = digsig
        
        return userData.jsonStringRepresentation!
    }
    
    func getQualificationCall(user: User, completionHandler: @escaping (_ done:Bool, _ qualifications:Dictionary<String, Any>) -> ()) {
        
        guard let postData = qualificationData(user:user).data(using: .utf8) else {
            self.statusCode = 1004
            return
        }
        
        var request = makeRequestHeader(postData: postData, endpointPath: Constants.endPointQualifications)
        let loginToken = PreferenceUtil.instance.get(key: PreferenceUtil.LOGIN_TOKEN)
        request.addValue("Bearer \(loginToken)", forHTTPHeaderField: "Authorization")
        
        checkDownloadedDataHasErrors(request: request) { returnedDictionary in
            
//            print("# getQualificationCall - returnedDictionary = \(String(describing: returnedDictionary))")
            
            if (returnedDictionary["code"] != nil && returnedDictionary["code"] as! Int != 200) {
                self.statusCode = returnedDictionary["code"] as! Int
            }
            else {
                self.statusCode = 200
            }

            completionHandler(true, returnedDictionary)
        }
    }
    
    private func registerQualificationData(user:User) -> String {
        
        var userData = [String: Any]()
        let id = Int64(Date().timeIntervalSince1970 * 1000)
        userData[Constants.apiKeyId] = String(id)
        userData[Constants.apiKeyExecer] = user.nic + Constants.execerSuffix
        userData[Constants.apiKeyMessageType] = Constants.messageTypeCreateQualification
        userData[Constants.apiKeyQualificationId] = String(id)
        userData[Constants.apiKeyQualificationHolder] = user.nic
        
        let olResultDictionary = [ Constants.apiKeyOlMathematics : user.olResults.mathematics,
                                   Constants.apiKeyOlEnglishg : user.olResults.english,
                                   Constants.apiKeyOlSinhala : user.olResults.sinhala,
                                   Constants.apiKeyOlTamil : user.olResults.tamil,
                                   Constants.apiKeyOlScience : user.olResults.science,
                                   Constants.apiKeyOlHealthScience : user.olResults.healthScience,
                                   Constants.apiKeyOlReligion : user.olResults.religion,
                                   Constants.apiKeyOlArt : user.olResults.art,
                                   Constants.apiKeyOlHistory : user.olResults.history,
                                   Constants.apiKeyOlGeography : user.olResults.geography]
        userData[Constants.apiKeyOlResult] = olResultDictionary

        let otherEduQualifiDict = [ Constants.apiKeyHasDiploma : user.otherEduQualifications.hasCompletedQualification.boolValue,
                                    Constants.apiKeyNameDiploma : user.otherEduQualifications.completedQualificationName] as [String : Any]
        userData[Constants.apiKeyEducationalQualification] = otherEduQualifiDict
        
        let profQualifiDict = [ Constants.apiKeyHasExperience : user.professionalQualifications.hasAnyProfExperience.boolValue,
                                Constants.apiKeyNameExperience : user.professionalQualifications.nameOfProfExperience,
                                Constants.apiKeyDurationExperience : user.professionalQualifications.durationOfProfExperience,
                                Constants.apiKeyProfessionArea : user.professionalQualifications.exerciseAreaProfession,
                                Constants.apiKeyProfessionName : user.professionalQualifications.nameOfProfExperience] as [String : Any]
        userData[Constants.apiKeyProfessionalQualification] = profQualifiDict
        
        guard let payload = CryptoUtil.instance.preparePayload(args: userData) else {
            self.statusCode = 1005
            return ""
        }
        let digsig = CryptoUtil.instance.sign(payload: payload)
        userData[Constants.apiKeyMsgSig] = digsig
        
        return userData.jsonStringRepresentation!
    }
    
    func registerQualificationCall(user:User, completionHandler: @escaping (_ done:Bool) -> ()) {
        
        guard let postData = registerQualificationData(user:user).data(using: .utf8) else {
            self.statusCode = 1004
            return
        }
        
        var request = makeRequestHeader(postData: postData, endpointPath: Constants.endPointQualifications)
        let loginToken = PreferenceUtil.instance.get(key: PreferenceUtil.LOGIN_TOKEN)
        request.addValue("Bearer \(loginToken)", forHTTPHeaderField: "Authorization")
        
        checkDownloadedDataHasErrors(request: request) { returnedDictionary in
            
//            print("# registerQualificationCall - returnedDictionary = \(String(describing: returnedDictionary))")
            
            if (returnedDictionary["code"] != nil) {
                self.statusCode = returnedDictionary["code"] as? Int ?? 0
                
                print(self.statusCode)
            }
            
            completionHandler(true)
        }
    }
}
