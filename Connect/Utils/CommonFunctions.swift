//
//  CommonFunctions.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-09-15.
//

import SwiftUI

func convertImageToBase64String (img: UIImage) -> String {
    return img.jpegData(compressionQuality: 0.25)?.base64EncodedString() ?? ""
}
