//
//  Constants.swift
//  Connect
//
//  Created by Arosha Piyadigama on 7/23/21.
//

import Foundation

struct Constants {
    static let genderArray: [String] = ["Male", "Female", "Other"]
    static let provinceArray: [String] = ["Central", "Eastern", "North Central", "North Western", "Northern", "Sabaragamuwa", "Southern", "Uva", "Western"]
    static let districtArray: [String] = ["Colombo", "Gampaha", "Kaluthara", "Galle", "Matara", "Hambanthota", "Kandy", "Matale", "Nuwara Eliya", "Badulla", "Monaragala", "Jaffna", "Batticalo", "Mulativ"]
    static let olResultArray: [String] = ["A", "B", "C", "F"]
    
    static let experienceTimeDurationsArray: [String] = ["0-3 Months", "4-6 Months", "7-12 Months", "1-2 Years", "3-4 Years", "More than 4 Years"]
    
    static let professionAreasArray: [String] = ["Sports Journalism", "Health Physical Fitness", "Recreational Sports", "Sporting Goods", "Sports Venue", "Tourism", "Sports Analyst", "Sports Team", "Other"]
    
    static let sportsJournalismProfessionArray: [String] = ["Editor", "Publisher", "Newspaper Sports Writer", "Online Sports Journalist", "Internet", "Sports Journalist", "Sports Photographer", "Radio Sports Producer", "Television Sports Producer"]
    
    static let healthPhysicalFitnessProfessionArray: [String] = ["Physical Therapist", "Medical Assistant", "Sports Medicine Aide", "Trainer", "Assistant Trainer", "Physical Therapist Assistant", "Sports Message Therapist", "Sports Physiotherapist", "Sports Nutritionist"]
    
    static let recreationalSportsProfessionArray: [String] = ["Youth Sports Administrator", "Recreational Manager", "Director", "Recreational Aid", "Recreational Specialist", "Recreational Therapist", "Outdoor Recreational Planner", "Teen Coordinator", "Instructor"]
    
    static let sportingGoodsProfessionArray: [String] = ["Manufacturer’s Representative", "Sporting Goods Store Manager", "Sporting Goods Salesperson", "Sports Store Business Owner", "Team Dealer", "Other"]
    
    static let sportsVenueProfessionArray: [String] = ["Relations With Other Organizations", "Finance manager", "Marketing manager", "Legal Affairs", "Technology Operator", "Operations"]
    
    static let tourismProfessionArray: [String] = ["Online Adjunct Professor", "Other"]
    
    static let sportsAnalystProfessionArray: [String] = ["Web Analytics Supervisor", "Associate Data Processor", "Supervising Editor", "Social Media Specialist", "Legislative Analyst", "Analytics Engagement Manager"]
    
    static let sportsTeamProfessionArray: [String] = ["Sports Coach", "Assistant Coach", "Physiotherapist", "Sports Marketing", "Trainer", "Assistant Trainer"]
    
    static let otherProfessionArray: [String] = ["PE Teacher", "Sports Lawyer", "Associate Athletic Director", "Physical Education Instructor", "Marketing and Promotions Coordinator", "Sports Program Development Director"]
    
    static let apiDateFormat:String = "YYYY-MM-dd"
    
    static let endPointIdentities:String = "identities"
    static let endPointQualifications:String = "qualifications"
    static let baseAPIUrlStr:String = "http://20.102.59.98:7654/api/"
    
    static let apiKeyId = "id"
    static let apiKeyExecer = "execer"
    static let apiKeyMessageType = "messageType"
    static let apiKeyDid = "did"
    static let apiKeyOwner = "owner"
    static let apiKeyPubkey = "pubkey"
    static let apiKeyPassword = "password"
    static let apiKeyRoles = "roles"
    static let apiKeyGroups = "groups"
    static let apiKeyName = "name"
    static let apiKeyGender = "gender"
    static let apiKeyAge = "age"
    static let apiKeyDob = "dob"
    static let apiKeyPhone = "phone"
    static let apiKeyEmail = "email"
    static let apiKeyAddress = "address"
    static let apiKeyProvince = "province"
    static let apiKeyDistrict = "district"
    static let apiKeyLat = "lat"
    static let apiKeyLon = "lon"
    static let apiKeyBlob = "blob"
    static let apiKeyDigSig = "digsig"
    static let apiKeyMsgSig = "msgsig"
    static let apiKeyQualificationHolder = "qualificationHolder"
    static let apiKeyQualificationId = "qualificationId"
    static let apiKeyOlResult = "olResult"
    static let apiKeyEducationalQualification = "educationalQualification"
    static let apiKeyProfessionalQualification = "professionalQualification"
    
    static let apiKeyOlMathematics = "olMathematics"
    static let apiKeyOlEnglishg = "olEnglish"
    static let apiKeyOlSinhala = "olSinhala"
    static let apiKeyOlTamil = "olTamil"
    static let apiKeyOlScience = "olScience"
    static let apiKeyOlHealthScience = "olHealthScience"
    static let apiKeyOlReligion = "olReligion"
    static let apiKeyOlArt = "olArt"
    static let apiKeyOlHistory = "olHistory"
    static let apiKeyOlGeography = "olGeography"
    
    static let apiKeyHasDiploma = "hasDiploma"
    static let apiKeyNameDiploma = "nameDiploma"
    
    static let apiKeyHasExperience = "hasExperience"
    static let apiKeyNameExperience = "nameExperience"
    static let apiKeyDurationExperience = "durationExperience"
    static let apiKeyProfessionArea = "professionArea"
    static let apiKeyProfessionName = "professionName"
    
    static let execerSuffix = ":ism"
    static let messageTypeCreate = "create"
    static let messageTypeConnect = "connect"
    static let messageTypeGet = "get"
    static let messageTypeGetQualifications = "getQualifications"
    static let messageTypeCreateQualification = "createQualification"
    static let ismOwner = "ism"
    
    static let qualificationId = "id"
    static let qualificationName = "name"
    static let qualificationOl = "ol"
    static let qualificationProf = "prof"
    
    static let digsig = "digsig"
    
    static let errorCodes: [Int:String] = [401:"Password or User ID is incorrect",
                                           404:"Qualifications not found",
                                           1001:"URLSession error",
                                           1002:"Response data is nil",
                                           1003:"JSONSerialization error",
                                           1004:"Error converting request data to utf8",
                                           1005:"Error preparing payload"]
}

