//
//  OLResults.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-09-11.
//

import Foundation

class OLResults: ObservableObject {
    @Published var mathematics = ""
    @Published var english = ""
    @Published var sinhala = ""
    @Published var tamil = ""
    @Published var science = ""
    @Published var healthScience = ""
    @Published var religion = ""
    @Published var art = ""
    @Published var history = ""
    @Published var geography = ""
}
