//
//  OtherEduQualifications.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-09-11.
//

import Foundation

class OtherEduQualifications: ObservableObject {
    @Published var hasCompletedQualification = ""
    @Published var completedQualificationName = ""
}
