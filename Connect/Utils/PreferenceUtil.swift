import Foundation

class PreferenceUtil {
    static let instance = PreferenceUtil()
    
    static let PUBLIC_KEY = "PUBLIC_KEY"
    static let PRIVATE_KEY = "PRIVATE_KEY"
    static let EMAIL = "EMAIL"
    static let PHONE = "PHONE"
    static let PASSWORD = "PASSWORD"
    static let DEVICE_ID = "DEVICE_ID"
    static let QUESTION1 = "QUESTION1"
    static let QUESTION2 = "QUESTION2"
    static let QUESTION3 = "QUESTION3"
    static let ACCOUNT = "ACCOUNT"
    static let ACCOUNT_STATUS = "ACCOUNT_STATUS"
    static let FCM_TOKEN = "FCM_TOKEN"
    static let UPDATE_FCM_TOKEN = "UPDATE_FCM_TOKEN"
    static let LOGIN_STATE = "LOGIN_STATE"
    static let LOGIN_TOKEN = "LOGIN_TOKEN"
    static let LAST_LOGIN_NIC = "LAST_LOGIN_NIC"
    
    let userDefaults = UserDefaults.standard
    
    func put(key: String, value: String) {
        userDefaults.set(value, forKey: key)
    }
    
    func get(key: String) -> String {
        if let highScore = userDefaults.value(forKey: key) {
            return highScore as! String
        }
        
        return ""
    }
}
