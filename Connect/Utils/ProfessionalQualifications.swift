//
//  ProfessionalQualifications.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-09-11.
//

import Foundation

class ProfessionalQualifications: ObservableObject {
    @Published var hasAnyProfExperience = ""
    @Published var nameOfProfExperience = ""
    @Published var durationOfProfExperience = ""
    @Published var exerciseArea = ""
    @Published var exerciseAreaProfession = ""
}
