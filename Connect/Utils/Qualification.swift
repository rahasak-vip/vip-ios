//
//  Qualification.swift
//  Connect
//
//  Created by eranga on 8/26/21.
//

import Foundation

class Qualification: ObservableObject {
    @Published var id = ""
    @Published var name = ""
    @Published var ol = ""
    @Published var prof = ""
}
