//
//  User.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-08-18.
//

import Foundation

class User: ObservableObject {
    
    @Published var nic = ""
    @Published var name = ""
    @Published var gender = ""
    @Published var age = 0
    @Published var dob = ""
    @Published var phoneNumber = ""
    @Published var email = ""
    @Published var province = ""
    @Published var district = ""
    @Published var address = ""
    @Published var passwordShaValue = ""
    @Published var blob = ""
    @Published var activated = false
    @Published var olResults = OLResults()
    @Published var otherEduQualifications = OtherEduQualifications()
    @Published var professionalQualifications = ProfessionalQualifications()
}
