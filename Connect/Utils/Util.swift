//
//  Util.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-08-23.
//

import Foundation
import CryptoKit

class Util {
    
    // Works only iOS 13.0+
    func convertToSha256(password:String) -> String {
        
        let inputString = password
        let inputData = Data(inputString.utf8)
        let hashed = SHA256.hash(data: inputData)
        return hashed.compactMap { String(format: "%02x", $0) }.joined()
    }
}
