//
//  AddQualificationsView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-07-31.
//

import SwiftUI

struct AddQualificationsView: View {
    var body: some View {
        VStack {
            Spacer()
            
            Text("You have successfully registered in the identityservice of ministry of youth and sports. \nPlease add your experience, educational qualifications and professional qualificationsto complete the profile")
                .padding(.all, 25)
            
            Spacer()
            
            Button(action: {
            }){
                NavigationLink(destination: OLQualificationsView()) {
                    RoundedGreenButtonText(text: "Next")
                }
            }
            .padding(.horizontal)
            
            Button(action: {
            }){
                NavigationLink(destination: HomeView()) {
                    RoundedGreenButtonText(text: "Cancel")
                }
            }
            .padding(.horizontal)
        }
        .navigationBarTitle("Add Qualifications", displayMode: .inline)
        .navigationBarBackButtonHidden(true)
    }
}

struct AddQualificationsView_Previews: PreviewProvider {
    static var previews: some View {
        AddQualificationsView()
    }
}
