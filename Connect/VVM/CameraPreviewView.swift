//
//  CameraPreviewView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 7/27/21.
//

import SwiftUI
import AVFoundation

struct CameraPreviewView: View {
    
    @State var image: UIImage?
    @State var didTapCapture: Bool = false
    @State var didSaveImage: Bool = false
    @Environment(\.colorScheme) var colorScheme
    
    var body: some View {
        ZStack(alignment: .bottom)  {
            CustomCameraRepresentable(image: self.$image, didTapCapture: $didTapCapture, didSaveImage: $didSaveImage)
            
            NavigationLink(destination: SelfieSignView(image: image), isActive: $didSaveImage) {
                CaptureButtonView()
                    .frame(maxWidth: .infinity, maxHeight: UIScreen.screenHeight/4)
                    .background(colorScheme == .dark ? Color.black : Color.white)
                    .opacity(/*@START_MENU_TOKEN@*/0.8/*@END_MENU_TOKEN@*/)
                    .onTapGesture {
                        self.didTapCapture = true
                    }
            }
        }
        .ignoresSafeArea()
        .navigationBarTitle("Take Selfie", displayMode: .inline)
    }
}

struct CameraPreviewView_Previews: PreviewProvider {
    static var previews: some View {
        CameraPreviewView()
    }
}
