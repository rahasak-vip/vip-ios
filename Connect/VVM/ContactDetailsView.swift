//
//  ContactDetailsView.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-10-14.
//

import SwiftUI

struct ContactDetailsView: View {
    
    private static let logoCornerRadius = 10.0
    
    var body: some View {
        ZStack {
            Color.white.ignoresSafeArea()
            
            VStack {
                Spacer()
                
                Image("SplashBgBottom")
                    .resizable()
                    .frame(width: UIScreen.screenWidth, height: UIScreen.screenHeight * 4/5)
                    .aspectRatio(contentMode: .fit)
            }
            .ignoresSafeArea()
            
            VStack {
                Image("SplashIcon")
                    .resizable()
                    .frame(width: 100, height: 100)
                    .clipShape(RoundedRectangle(cornerRadius: ContactDetailsView.logoCornerRadius))
                    .overlay(RoundedRectangle(cornerRadius: ContactDetailsView.logoCornerRadius).stroke(Color.white, lineWidth: 2))
                
                Text("Address")
                    .fontWeight(.bold)
                    .padding([.top, .leading, .trailing])
                
                Text("No 33, Maitland Place,\n Colombo 07")
                    .fontWeight(.light)
                    .multilineTextAlignment(.center)
                
                Text("Phone")
                    .fontWeight(.bold)
                    .padding([.top, .leading, .trailing])
                
                Text("011 269 5953")
                    .fontWeight(.light)
                
                Text("Email")
                    .fontWeight(.bold)
                    .padding([.top, .leading, .trailing])
                
                Text("ism_medicine@yahoo.com")
                    .fontWeight(.light)
                    .accentColor(Color.black)
                
                Image("ContactDetails")
                    .resizable()
                    .frame(width: 250, height: 250)
            }
        }
    }
}

struct ContactDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        ContactDetailsView()
    }
}
