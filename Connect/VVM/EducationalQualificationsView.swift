//
//  EducationalQualificationsView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-07-31.
//

import SwiftUI

struct EducationalQualificationsView: View {
    
    @State var hasCompletedQualification: String = ""
    @State var completedQualificationName: String = ""
    @State var isFormFilled = false
    @StateObject var educationalQualificationsViewModel = EducationalQualificationsViewModel()
    @EnvironmentObject var user: User
    
    var body: some View {
        VStack(alignment: .leading) {
            Spacer()
            
            Text("1. Did you complete a deploma/degree/certification related to sports and exercise?")
                .padding(.all, 25)
            
            Picker(selection: $hasCompletedQualification,
                   label: GreenLineTextView(title: "Yes", text: $hasCompletedQualification)) {
                ForEach(["Yes", "No"], id: \.self) { result in
                    Text(result)
                        .tag(result)
                }
            }
            .pickerStyle(MenuPickerStyle())
            
            Text("2. If yes, mention the course here")
                .padding(.all, 25)
            GreenLineTextView(title: "Detail", text: $completedQualificationName, autoCapType: .sentences)
            
            Spacer()
            
            NavigationLink(destination: ProfessionalQualificationsView(),
                           isActive: $isFormFilled) {
                Button(action: {
                    
                    if educationalQualificationsViewModel.isRequiredFieldsFilled(hasCompletedQualification: hasCompletedQualification, completedQualificationName: completedQualificationName) {
                        isFormFilled = true
                        user.otherEduQualifications.hasCompletedQualification = hasCompletedQualification
                        user.otherEduQualifications.completedQualificationName = completedQualificationName
                    }
                }){
                    RoundedGreenButtonText(text: "Next")
                }
                .padding(.horizontal)
                .alert(isPresented: $educationalQualificationsViewModel.showsAlert) { () -> Alert in
                    Alert(title: Text(educationalQualificationsViewModel.alertMessage))
                }
            }
            
        }
        .navigationBarTitle("Educational Qualifications", displayMode: .inline)
    }
}

struct EducationalQualificationsView_Previews: PreviewProvider {
    static var previews: some View {
        EducationalQualificationsView()
    }
}
