//
//  EducationalQualificationsViewModel.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-09-11.
//

import Foundation

class EducationalQualificationsViewModel: ObservableObject {
    
    @Published var showsAlert = false
    @Published var alertMessage = ""
    
    func isRequiredFieldsFilled(hasCompletedQualification:String, completedQualificationName:String) -> Bool {

        if hasCompletedQualification.isEmpty || completedQualificationName.isEmpty {
            self.showsAlert = true
            self.alertMessage = "You need to fill all fields to complete the registration"
            return false
        }
        
        return true
    }
}
