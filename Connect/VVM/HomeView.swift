//
//  HomeView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-07-31.
//

import SwiftUI

struct HomeView: View {
    
    @State var selected = "Messages"
    var tabItems = ["Identity", "Qualifications", "Preference"]
    @EnvironmentObject var user: User
    @StateObject var apiProxy = APIProxy()
    @StateObject var homeViewModel = HomeViewModel()
    @State private var isLoading = true
    
    var body: some View {
        ZStack {
            VStack {
                TabView(selection: $selected) {
                    IdentityView()
                        .tabItem {
                            Image(systemName: "person.circle")
                            Text("Identity")
                        }
                        .tag(tabItems[0])
                    QualificationsView()
                        .tabItem {
                            Image(systemName: "paperplane")
                            Text("Qualifications")
                        }
                        .tag(tabItems[1])
                    PreferenceView()
                        .tabItem {
                            Image(systemName: "gear")
                            Text("Preference")
                        }
                        .tag(tabItems[2])
                }
            }
            .navigationBarTitle(self.selected, displayMode: .inline)
            .navigationBarBackButtonHidden(true)
            .onAppear() {
                apiProxy.getUserData(user: user) { (returnedData, returnedUser)  in
                    
                    DispatchQueue.main.async {
                        if apiProxy.statusCode == 200 {
                            homeViewModel.updateUserDetails(envUser: user, loggedUser: returnedUser)
                        }
                        else {
                            homeViewModel.somethingIsWrong(code: apiProxy.statusCode)
                        }
                        
                        isLoading = false
                    }   
                }
            }
            .alert(isPresented: $homeViewModel.showsAlert) { () -> Alert in
                Alert(title: Text(homeViewModel.alertMessage))
            }
            
            if isLoading {
                CustomProgressView()
            }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
