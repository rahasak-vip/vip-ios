//
//  HomeViewModel.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-08-31.
//

import Foundation

class HomeViewModel: ObservableObject {
    
    @Published var showsAlert = false
    @Published var alertMessage = ""
    
    func updateUserDetails(envUser: User, loggedUser: User) {
        envUser.nic = loggedUser.nic
        envUser.name = loggedUser.name
        envUser.dob = loggedUser.dob
        envUser.phoneNumber = loggedUser.phoneNumber
        envUser.email = loggedUser.email
        envUser.blob = loggedUser.blob
        envUser.activated = loggedUser.activated
    }
    
    func somethingIsWrong(code: Int) {
        print("# code = \(code)")
        
        self.showsAlert = true
        
        if Constants.errorCodes.keys.contains(code) {
            self.alertMessage = Constants.errorCodes[code]!
        }
        else {
            self.alertMessage = "Something has gone wrong"
        }
    }
}
