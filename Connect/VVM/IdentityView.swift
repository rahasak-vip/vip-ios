//
//  IdentityView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-07-31.
//

import SwiftUI

struct IdentityView: View {
    
    @EnvironmentObject var user: User
    
    var body: some View {
        ZStack {
            VStack {
                Image("HeadBanner")
                    .resizable()
                    .frame(width: UIScreen.screenWidth, height: UIScreen.screenHeight/3)
                    .aspectRatio(contentMode: .fit)
                Spacer()
                
                List {
                    PreferencesCellView(key: "NIC", value: user.nic)
                    PreferencesCellView(key: "Name", value: user.name)
                    PreferencesCellView(key: "Phone", value: user.phoneNumber)
                    PreferencesCellView(key: "Identity status", value: user.activated ? "Verified" : "Not verified")
                }
                .position(x: UIScreen.screenWidth/2, y: UIScreen.screenHeight/4 + 60)
            }
            
            if !user.blob.isEmpty {
                Image(uiImage: user.blob.toImage()!)
                    .resizable()
                    .frame(width: 100.0, height: 100.0)
                    .clipShape(Circle())
                    .position(x: 60.0, y: UIScreen.screenHeight/3)
            }
            else {
                Image(systemName: "person.circle.fill")
                    .resizable()
                    .frame(width: 100.0, height: 100.0)
                    .background(Color.gray)
                    .foregroundColor(.white)
                    .clipShape(Circle())
                    .position(x: 60.0, y: UIScreen.screenHeight/3)
            }
        }
    }
}

struct IdentityView_Previews: PreviewProvider {
    static var previews: some View {
        IdentityView()
    }
}
