//
//  LoginView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 7/20/21.
//

import SwiftUI

struct LoginView: View {
    
    @State var nic: String = ""
    @State var password: String = ""
    @StateObject var loginViewModel = LoginViewModel()
    @StateObject var user = User()
    @StateObject var apiProxy = APIProxy()
    @State private var isLoading = false
    @State private var navigationSelection: String? = nil
    let navSelectionHome = "Home"
    let navSelectionAddQualifications = "AddQualifications"
    
    var body: some View {
        NavigationView {
            ZStack {
                VStack {
                    
                    Spacer()
                    
                    GreenLineTextView(title: "NIC", text: $nic, autoCapType: .allCharacters)
                    
                    GreenLineSecureField(title: "Password", text: $password)
                    
                    NavigationLink(destination: RegisterPersonalDetailsView()) {
                        GrayUnderlinedText(text: "Don't have an account? Register")
                    }
                    
                    //                Button(action: {
                    //                    // Button action
                    //                }){
                    //                    GrayUnderlinedText(text: "Forgot password")
                    //                }
                    
                    Spacer()
                    
                    NavigationLink(destination: HomeView(), tag: navSelectionHome, selection: $navigationSelection) { EmptyView() }
                    NavigationLink(destination: AddQualificationsView(), tag: navSelectionAddQualifications, selection: $navigationSelection) { EmptyView() }
                    
                    Button(action: {
                        
                        if loginViewModel.isRequiredFieldsFilled(nic: nic, password: password, isConnected: apiProxy.isConnected) {
                            hideKeyboard()
                            user.nic = nic
                            user.passwordShaValue = Util().convertToSha256(password: password)
                            
                            isLoading = true
                            
                            apiProxy.connectCall(user: user) { (returnedData) in
                                
                                isLoading = false
                                
                                if apiProxy.statusCode == 200  {
                                    self.navigationSelection = navSelectionHome
                                }
                                else if apiProxy.statusCode == 405 {
                                    self.navigationSelection = navSelectionAddQualifications
                                }
                                else {
                                    DispatchQueue.main.async {
                                        loginViewModel.somethingIsWrong(code: apiProxy.statusCode)
                                    }
                                }
                            }
                        }
                    }){
                        RoundedGreenButtonText(text: "Login")
                    }
                    .padding(.horizontal)
                    .alert(isPresented: $loginViewModel.showsAlert) { () -> Alert in
                        Alert(title: Text(loginViewModel.alertMessage))
                    }
                }
                .onAppear(){
                    apiProxy.isConnectedToInternet()
                    nic = loginViewModel.savedLoginNIC()
                }
                
                if isLoading {
                    CustomProgressView()
                }
            }
        }
        .navigationBarHidden(true)
        .environmentObject(user)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
