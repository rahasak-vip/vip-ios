//
//  LoginViewModel.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-08-07.
//

import Foundation

class LoginViewModel: ObservableObject {
    
    @Published var showsAlert = false
    @Published var alertMessage = ""
    
    func isRequiredFieldsFilled(nic:String, password:String, isConnected:Bool) -> Bool {
        
        if nic.isEmpty || password.isEmpty {
            self.showsAlert = true
            self.alertMessage = "Please enter NIC and Password to log in"
            return false
        }
        else if !isConnected {
            self.showsAlert = true
            self.alertMessage = "You need to connect to the Internet"
            return false
        }
        
        return true
    }
    
    func somethingIsWrong(code: Int) {
        print("# code = \(code)")
        
        self.showsAlert = true
        
        if Constants.errorCodes.keys.contains(code) {
            self.alertMessage = Constants.errorCodes[code]!
        }
        else {
            self.alertMessage = "Something has gone wrong"
        }
    }
    
    func savedLoginNIC() -> String {
        
        let savedNic = PreferenceUtil.instance.get(key: PreferenceUtil.LAST_LOGIN_NIC)
        return savedNic
    }
}
