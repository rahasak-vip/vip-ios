//
//  OLQualificationsView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-08-09.
//

import SwiftUI

struct OLQualificationsView: View {
    
    @State var mathematics = ""
    @State var english = ""
    @State var sinhala = ""
    @State var tamil = ""
    @State var science = ""
    @State var healthScience = ""
    @State var religion = ""
    @State var art = ""
    @State var history = ""
    @State var geography = ""
    @StateObject var olQualificationsViewModel = OLQualificationsViewModel()
    @State var isFormFilled = false
    @EnvironmentObject var user: User
    
    var body: some View {
        ScrollView {
            LazyVStack {
                Picker(selection: $mathematics,
                       label: GreenLineTextView(title: "Matematics", text: $mathematics)) {
                    ForEach(Constants.olResultArray, id: \.self) { result in
                        Text(result)
                            .tag(result)
                    }
                }
                .pickerStyle(MenuPickerStyle())
                
                Picker(selection: $english,
                       label: GreenLineTextView(title: "English", text: $english)) {
                    ForEach(Constants.olResultArray, id: \.self) { result in
                        Text(result)
                            .tag(result)
                    }
                }
                .pickerStyle(MenuPickerStyle())
                
                Picker(selection: $sinhala,
                       label: GreenLineTextView(title: "Sinhala", text: $sinhala)) {
                    ForEach(Constants.olResultArray, id: \.self) { result in
                        Text(result)
                            .tag(result)
                    }
                }
                .pickerStyle(MenuPickerStyle())
                
                Picker(selection: $tamil,
                       label: GreenLineTextView(title: "Tamil", text: $tamil)) {
                    ForEach(Constants.olResultArray, id: \.self) { result in
                        Text(result)
                            .tag(result)
                    }
                }
                .pickerStyle(MenuPickerStyle())
                
                Picker(selection: $science,
                       label: GreenLineTextView(title: "Science", text: $science)) {
                    ForEach(Constants.olResultArray, id: \.self) { result in
                        Text(result)
                            .tag(result)
                    }
                }
                .pickerStyle(MenuPickerStyle())
                
                Picker(selection: $healthScience,
                       label: GreenLineTextView(title: "Health Science", text: $healthScience)) {
                    ForEach(Constants.olResultArray, id: \.self) { result in
                        Text(result)
                            .tag(result)
                    }
                }
                .pickerStyle(MenuPickerStyle())
                
                Picker(selection: $religion,
                       label: GreenLineTextView(title: "Religion", text: $religion)) {
                    ForEach(Constants.olResultArray, id: \.self) { result in
                        Text(result)
                            .tag(result)
                    }
                }
                .pickerStyle(MenuPickerStyle())
                
                Picker(selection: $art,
                       label: GreenLineTextView(title: "Art", text: $art)) {
                    ForEach(Constants.olResultArray, id: \.self) { result in
                        Text(result)
                            .tag(result)
                    }
                }
                .pickerStyle(MenuPickerStyle())
                
                Picker(selection: $history,
                       label: GreenLineTextView(title: "History", text: $history)) {
                    ForEach(Constants.olResultArray, id: \.self) { result in
                        Text(result)
                            .tag(result)
                    }
                }
                .pickerStyle(MenuPickerStyle())
                
                Picker(selection: $geography,
                       label: GreenLineTextView(title: "Geography", text: $geography)) {
                    ForEach(Constants.olResultArray, id: \.self) { result in
                        Text(result)
                            .tag(result)
                    }
                }
                .pickerStyle(MenuPickerStyle())
            }
            
            NavigationLink(destination: EducationalQualificationsView(),
                           isActive: $isFormFilled) {
                Button(action: {
                    
                    if olQualificationsViewModel.isRequiredFieldsFilled(mathematics: mathematics, english: english, sinhala: sinhala, tamil: tamil, science: science, healthScience: healthScience, religion: religion, art: art, history: history, geography: geography) {
                        isFormFilled = true
                        user.olResults.mathematics = mathematics
                        user.olResults.english = english
                        user.olResults.sinhala = sinhala
                        user.olResults.tamil = tamil
                        user.olResults.science = science
                        user.olResults.healthScience = healthScience
                        user.olResults.religion = religion
                        user.olResults.art = art
                        user.olResults.history = history
                        user.olResults.geography = geography
                    }
                }){
                    RoundedGreenButtonText(text: "Next")
                }
                .padding(.horizontal)
                .alert(isPresented: $olQualificationsViewModel.showsAlert) { () -> Alert in
                    Alert(title: Text(olQualificationsViewModel.alertMessage))
                }
            }
            
        }
        .padding(.top)
        .navigationBarTitle("Educational Qualifications (O/L)", displayMode: .inline)
    }
}

struct OLQualificationsView_Previews: PreviewProvider {
    static var previews: some View {
        OLQualificationsView()
    }
}
