//
//  OLQualificationsViewModel.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-08-11.
//

import Foundation

class OLQualificationsViewModel: ObservableObject {
    
    @Published var showsAlert = false
    @Published var alertMessage = ""
    
    func isRequiredFieldsFilled(mathematics:String, english:String, sinhala:String, tamil:String, science:String, healthScience:String, religion:String, art:String, history:String, geography:String) -> Bool {

        if mathematics.isEmpty || english.isEmpty || sinhala.isEmpty || tamil.isEmpty || science.isEmpty || healthScience.isEmpty || religion.isEmpty || art.isEmpty || history.isEmpty || geography.isEmpty{
            self.showsAlert = true
            self.alertMessage = "You need to fill all fields to complete the registration"
            return false
        }
        
        return true
    }
}
