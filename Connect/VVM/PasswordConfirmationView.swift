//
//  PasswordConfirmationView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-07-31.
//

import SwiftUI

struct PasswordConfirmationView: View {
    
    @State var password: String = ""
    @State var confirmPassword: String = ""
    @State var isActive = false
    @StateObject var passwordConfirmationViewModel = PasswordConfirmationViewModel()
    @State var isFormFilled = false
    @EnvironmentObject var user: User
    @StateObject var apiProxy = APIProxy()
    @State private var isLoading = false
    
    var body: some View {
        ZStack {
            VStack {
                Spacer()
                GreenLineSecureField(title: "Password", text: $password)
                GreenLineSecureField(title: "Confirm Password", text: $confirmPassword)
                Text("By registering you agree to our")
                    .font(.subheadline)
                    .foregroundColor(.gray)
                    .padding(.bottom, 5)
                
                NavigationLink(destination: TermsAndConditionsView()) {
                    GrayUnderlinedText(text: "Terms and Conditions")
                }
                
                Spacer()
                
                NavigationLink(destination: AddQualificationsView(),
                               isActive: $isFormFilled) {
                    Button(action: {
                        
                        if passwordConfirmationViewModel.isRequiredFieldsFilled(password: password, confirmPassword: confirmPassword, isConnected: apiProxy.isConnected) {
                            user.passwordShaValue = Util().convertToSha256(password: password)
                            
                            if(CryptoUtil.instance.initKeys()) {
                                isLoading = true
                                
                                apiProxy.registerCall(user: user) { returnedData in
                                    DispatchQueue.main.async {
                                        isLoading = false
                                        if apiProxy.statusCode == 201 {
                                            isFormFilled = true
                                        }
                                        else {
                                            passwordConfirmationViewModel.somethingIsWrong(code: apiProxy.statusCode)
                                        }
                                    }
                                }
                            }
                            else {
                                passwordConfirmationViewModel.somethingIsWrong(code: apiProxy.statusCode)
                            }
                        }
                    }){
                        RoundedGreenButtonText(text: "DONE")
                    }
                    .padding(.horizontal)
                    .alert(isPresented: $passwordConfirmationViewModel.showsAlert) { () -> Alert in
                        Alert(title: Text(passwordConfirmationViewModel.alertMessage))
                    }
                }
            }
            .navigationBarTitle("Password", displayMode: .inline)
            .onAppear(){
                apiProxy.isConnectedToInternet()
            }
            
            if isLoading {
                CustomProgressView()
            }
        }
    }
}

struct PasswordConfirmationView_Previews: PreviewProvider {
    static var previews: some View {
        PasswordConfirmationView()
    }
}
