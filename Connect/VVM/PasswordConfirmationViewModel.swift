//
//  PasswordConfirmationViewModel.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-08-09.
//

import Foundation

class PasswordConfirmationViewModel: ObservableObject {
    
    @Published var showsAlert = false
    @Published var alertMessage = ""
    
    func isRequiredFieldsFilled(password:String, confirmPassword:String, isConnected:Bool) -> Bool {

        if password.isEmpty || confirmPassword.isEmpty {
            self.showsAlert = true
            self.alertMessage = "You need to fill all fields to complete the registration"
            return false
        }
        else if password != confirmPassword {
            self.showsAlert = true
            self.alertMessage = "Password and Confirmed Password are mismatched"
            return false
        }
        else if password.count < 7 {
            self.showsAlert = true
            self.alertMessage = "Password should contain more than 7 characters"
            return false
        }
        else if !isConnected {
            self.showsAlert = true
            self.alertMessage = "You need to connect to the Internet"
            return false
        }
        
        return true
    }
    
    func somethingIsWrong(code: Int) {
        print("# code = \(code)")
        
        self.showsAlert = true
        
        if Constants.errorCodes.keys.contains(code) {
            self.alertMessage = Constants.errorCodes[code]!
        }
        else {
            self.alertMessage = "Something has gone wrong"
        }
    }
}
