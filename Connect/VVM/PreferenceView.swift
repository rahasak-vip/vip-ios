//
//  PreferenceView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-07-31.
//

import SwiftUI

struct PreferenceView: View {
    
    @EnvironmentObject var user: User
    @State var isPresented = false
    
    var body: some View {
        List {
            
            Text("Change password")
                .padding(.all)
                .modifier(TextFontModifier())
            
            NavigationLink(destination: TermsAndConditionsView()) {
                Text("Terms of use")
                    .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                    .modifier(TextFontModifier())
            }
            
            NavigationLink(destination: ContactDetailsView()) {
                Text("Contact details")
                    .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                    .modifier(TextFontModifier())
            }
            
            NavigationLink(destination: LoginView()) {
                Text("Exit \(Bundle.appDisplayName())")
                    .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                    .modifier(TextFontModifier())
            }
        }
    }
}

struct PreferenceView_Previews: PreviewProvider {
    static var previews: some View {
        PreferenceView()
    }
}
