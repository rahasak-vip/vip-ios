//
//  ProfessionalQualificationsView.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-09-11.
//

import SwiftUI

struct ProfessionalQualificationsView: View {
    
    @State var hasAnyProfExperience: String = ""
    @State var nameOfProfExperience: String = ""
    @State var durationOfProfExperience: String = ""
    @State var exerciseArea: String = ""
    @State var exerciseAreaProfession: String = ""
    @State var isFormFilled = false
    @StateObject var professionalQualificationsViewModel = ProfessionalQualificationsViewModel()
    @EnvironmentObject var user: User
    @StateObject var apiProxy = APIProxy()
    
    var body: some View {
        VStack(alignment: .leading) {
            ScrollView {
                Text("1. Do you have any professional experience in sports and exercise?")
                    .padding([.leading, .trailing, .top], 40)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .fixedSize(horizontal: false, vertical: true)
                CustomMenuPickerView(selection: $hasAnyProfExperience, title: "", dataArray: ["Yes", "No"])
                
                Text("2. If yes, mention the experience here")
                    .padding([.leading, .trailing], 40)
                    .frame(maxWidth: .infinity, alignment: .leading)
                GreenLineTextView(title: "", text: $nameOfProfExperience, autoCapType: .sentences)
                
                Text("3. Provide the time duration of the experience you had")
                    .padding([.leading, .trailing], 40)
                    .frame(maxWidth: .infinity, alignment: .leading)
                CustomMenuPickerView(selection: $durationOfProfExperience, title: "", dataArray: Constants.experienceTimeDurationsArray)
                
                Text("4. If you currently engage in an exercise profession, provide the area you engaged in")
                    .padding([.leading, .trailing], 40)
                    .frame(maxWidth: .infinity, alignment: .leading)
                CustomMenuPickerView(selection: $exerciseArea, title: "", dataArray: Constants.professionAreasArray)
                    .onChange(of: exerciseArea, perform: { value in
                        exerciseAreaProfession = ""
                    })
                
                Text("5. Provide the profession you engaged in")
                    .padding([.leading, .trailing], 40)
                    .frame(maxWidth: .infinity, alignment: .leading)
                CustomMenuPickerView(selection: $exerciseAreaProfession, title: "", dataArray: professionalQualificationsViewModel.filterRelevantProfession(professionArea: exerciseArea))
            }
            
            NavigationLink(destination: HomeView(), isActive: $isFormFilled) {
                Button(action: {
                    if professionalQualificationsViewModel.isRequiredFieldsFilled(hasAnyProfExperience: hasAnyProfExperience, nameOfProfExperience: nameOfProfExperience, durationOfProfExperience: durationOfProfExperience, exerciseArea: exerciseArea, exerciseAreaProfession: exerciseAreaProfession) {
                        
                        user.professionalQualifications.hasAnyProfExperience = hasAnyProfExperience
                        user.professionalQualifications.nameOfProfExperience = nameOfProfExperience
                        user.professionalQualifications.durationOfProfExperience = durationOfProfExperience
                        user.professionalQualifications.exerciseArea = exerciseArea
                        user.professionalQualifications.exerciseAreaProfession = exerciseAreaProfession
                        
                        apiProxy.registerQualificationCall(user: user) { returnedData in
                            print(returnedData)
                            isFormFilled = true
                        }
                    }
                }){
                    RoundedGreenButtonText(text: "Done")
                }
                .padding(.horizontal)
                .alert(isPresented: $professionalQualificationsViewModel.showsAlert) { () -> Alert in
                    Alert(title: Text(professionalQualificationsViewModel.alertMessage))
                }
            }
        }
        .navigationBarTitle("Professional Qualifications", displayMode: .inline)
    }
}

struct ProfessionalQualificationsView_Previews: PreviewProvider {
    static var previews: some View {
        ProfessionalQualificationsView()
    }
}
