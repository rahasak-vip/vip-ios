//
//  ProfessionalQualificationsViewModel.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-09-11.
//

import Foundation

class ProfessionalQualificationsViewModel: ObservableObject {
    
    @Published var showsAlert = false
    @Published var alertMessage = ""
    
    func isRequiredFieldsFilled(hasAnyProfExperience:String, nameOfProfExperience:String, durationOfProfExperience:String, exerciseArea:String, exerciseAreaProfession:String) -> Bool {

        if hasAnyProfExperience.isEmpty || nameOfProfExperience.isEmpty || durationOfProfExperience.isEmpty || exerciseArea.isEmpty || exerciseAreaProfession.isEmpty {
            self.showsAlert = true
            self.alertMessage = "You need to fill all fields to complete the registration"
            return false
        }
        
        return true
    }
    
    func filterRelevantProfession(professionArea: String) -> [String] {
        
        var professions: [String]
        
        switch professionArea {
        case "Sports Journalism":
            professions = Constants.sportsJournalismProfessionArray
        case "Health Physical Fitness":
            professions = Constants.healthPhysicalFitnessProfessionArray
        case "Recreational Sports":
            professions = Constants.recreationalSportsProfessionArray
        case "Sporting Goods":
            professions = Constants.sportingGoodsProfessionArray
        case "Sports Venue":
            professions = Constants.sportsVenueProfessionArray
        case "Tourism":
            professions = Constants.tourismProfessionArray
        case "Sports Analyst":
            professions = Constants.sportsAnalystProfessionArray
        case "Sports Team":
            professions = Constants.sportsTeamProfessionArray
        case "Other":
            professions = Constants.otherProfessionArray
        default:
            professions = [""]
        }
        
        return professions
    }
}
