//
//  QualificationsView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-07-31.
//

import SwiftUI

struct QualificationsView: View {
    
    @StateObject var apiProxy = APIProxy()
    @EnvironmentObject var user: User
    @State private var isLoading = true
    @StateObject var qualificationsViewModel = QualificationsViewModel()
    
    var body: some View {
        ZStack {

            List (qualificationsViewModel.qualificationsArray, id: \.self) { item in
                
                switch item {
                case "Educational Qualification":
                    QualificationCellView(qualifyHeading: item,
                                          qualifySubHeading: qualificationsViewModel.eduDiplomaName,
                                          qualifyTime: qualificationsViewModel.qualiTime)
                case "Professional Qualification":
                    QualificationCellView(qualifyHeading: item,
                                          qualifySubHeading: qualificationsViewModel.profQualiExpName,
                                          qualifyTime: qualificationsViewModel.qualiTime)
                default:
                    QualificationCellView(qualifyHeading: item,
                                          qualifySubHeading: "Ordinary Level Exam",
                                          qualifyTime: qualificationsViewModel.qualiTime)
                }
            }
            .onAppear() {
                apiProxy.getQualificationCall(user: user) { returnedData, returnedQualications in
                    
                    if apiProxy.statusCode == 404 {
                        DispatchQueue.main.async {
                            qualificationsViewModel.somethingIsWrong(code: apiProxy.statusCode)
                        }
                    }
                    
                    DispatchQueue.main.async {
                        qualificationsViewModel.updateQualificationsDetails(paramQualify: returnedQualications)
                        isLoading = false
                    }
                }
            }
            .alert(isPresented: $qualificationsViewModel.showsAlert) { () -> Alert in
                Alert(title: Text(qualificationsViewModel.alertMessage))
            }
            
            if isLoading {
                CustomProgressView()
            }
        }
    }
}

struct QualificationsView_Previews: PreviewProvider {
    static var previews: some View {
        QualificationsView()
    }
}
