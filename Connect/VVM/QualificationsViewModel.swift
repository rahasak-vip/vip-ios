//
//  QualificationsViewModel.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-09-10.
//

import Foundation

// TODO : Needs to reorganize this model
class QualificationsViewModel: ObservableObject {
    
    @Published var qualificationsArray:[String] = []
    @Published var qualiTime = ""
    @Published var eduDiplomaName = ""
    @Published var profQualiExpName = ""
    @Published var showsAlert = false
    @Published var alertMessage = ""
    
    func updateQualificationsDetails(paramQualify:Dictionary<String, Any>) {
        
        qualificationsArray.removeAll()
        
        if paramQualify["olResult"] != nil {
            qualificationsArray.append("OLevel Qualification")
        }
        
        if paramQualify["educationalQualification"] != nil {
            qualificationsArray.append("Educational Qualification")
            let eduQualifyDict = paramQualify["educationalQualification"] as! Dictionary<String, Any>
            eduDiplomaName = eduQualifyDict["nameDiploma"] as! String
        }
        
        if paramQualify["professionalQualification"] != nil {
            qualificationsArray.append("Professional Qualification")
            let profQualifyDict = paramQualify["professionalQualification"] as! Dictionary<String, Any>
            profQualiExpName = profQualifyDict["nameExperience"] as! String
        }
        
        if paramQualify["timestamp"] != nil {
            let returnedTime = paramQualify["timestamp"] as! String
            let returnedTimeArray = returnedTime.components(separatedBy: " ")
            qualiTime = returnedTimeArray[0]
        }
    }
    
    func somethingIsWrong(code: Int) {
        print("# code = \(code)")
        
        self.showsAlert = true
        
        if Constants.errorCodes.keys.contains(code) {
            self.alertMessage = Constants.errorCodes[code]!
        }
        else {
            self.alertMessage = "Something has gone wrong"
        }
    }
}

// MARK: - ReturnedQualifications
struct ReturnedQualifications: Codable {
    let educationalQualification: EducationalQualification
    let holder, id: String
    let olResult: OlResult
    let professionalQualification: ProfessionalQualification
    let status, timestamp: String
}

// MARK: - EducationalQualification
struct EducationalQualification: Codable {
    let hasDiploma: Bool
    let nameDiploma: String
}

// MARK: - OlResult
struct OlResult: Codable {
    let olArt, olEnglish, olHealthScience, olHistory: String
    let olMathematics, olReligion, olScience, olSinhala: String
    let olTamil: String
}

// MARK: - ProfessionalQualification
struct ProfessionalQualification: Codable {
    let durationExperience: String
    let hasExperience: Bool
    let nameExperience, professionArea, professionName: String
}
