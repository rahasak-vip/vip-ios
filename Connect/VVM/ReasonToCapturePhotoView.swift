//
//  ReasonToCapturePhotoView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 7/24/21.
//

import SwiftUI

struct ReasonToCapturePhotoView: View {
    
    @State var isCamearaPermissionGiven = false
    @StateObject var reasonToCapturePhotoViewModel = ReasonToCapturePhotoViewModel()
    
    var body: some View {
        VStack {
            Spacer()
            
            Text("To complete the registration you need to capture your photo \nThis image will be used to verify your identity at the admin verification process")
                .padding(.all, 25)
            
            Spacer()
            
            NavigationLink(destination: CameraPreviewView(), isActive: $isCamearaPermissionGiven) {
                Button(action: {
                    reasonToCapturePhotoViewModel.checkCameraPermission { camAuthStatus in
                        
                        if camAuthStatus == .authorized {
                            isCamearaPermissionGiven = true
                        }
                    }
                })
                {
                    RoundedGreenButtonText(text: "Next")
                }
                .padding(.horizontal)
                .alert(isPresented: $reasonToCapturePhotoViewModel.showsAlert) { () -> Alert in
                    Alert(title: Text(reasonToCapturePhotoViewModel.alertMessage))
                }
            }
        }
        .navigationBarTitle("Capture Photo", displayMode: .inline)
    }
}

struct ReasonToCapturePhotoView_Previews: PreviewProvider {
    static var previews: some View {
        ReasonToCapturePhotoView()
    }
}
