//
//  ReasonToCapturePhotoViewModel.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-10-01.
//

import Foundation
import AVFoundation

class ReasonToCapturePhotoViewModel: ObservableObject {
    
    @Published var showsAlert = false
    @Published var alertMessage = ""
    
    func checkCameraPermission(completion: @escaping (AVAuthorizationStatus) -> ()) {
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            completion(.authorized)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { (status) in
                
                if status {
                    completion(.authorized)
                }
            }
        case .denied:
            self.showsAlert = true
            self.alertMessage = "You have denied accessing camera. Please allow the permission by going to application Settings"
            completion(.denied)
            return
        default:
            completion(.restricted)
            return
        }
    }
}
