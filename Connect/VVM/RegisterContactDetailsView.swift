//
//  RegisterContactDetailsView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 7/23/21.
//

import SwiftUI

struct RegisterContactDetailsView: View {
    
    @State var phoneNumber: String = ""
    @State var email: String = ""
    @State var address: String = ""
    @State private var selectedProvince = ""
    @State private var selectedDistrict = ""
    @Environment(\.colorScheme) var colorScheme
    @StateObject var registerContactDetailsViewModel = RegisterContactDetailsViewModel()
    @State var isFormFilled = false
    @EnvironmentObject var user: User
    
    var body: some View {
        VStack {
            Spacer()
            GreenLineTextView(title: "Phone Number", text: $phoneNumber, keyboardType: .phonePad)
                .textContentType(.telephoneNumber)
            
            GreenLineTextView(title: "Email", text: $email, keyboardType: .emailAddress)
            
            Picker(selection: $selectedProvince,
                   label: GreenLineTextView(title: "Province", text: $selectedProvince)
                    .onChange(of: selectedProvince, perform: { value in
                        selectedDistrict = ""
                    })
            ) {
                ForEach(Constants.provinceArray, id: \.self) { province in
                    Text(province)
                        .tag(province)
                }
            }
            .pickerStyle(MenuPickerStyle())
            
            Picker(selection: $selectedDistrict,
                   label: GreenLineTextView(title: "District", text: $selectedDistrict)
            ) {
                ForEach(registerContactDetailsViewModel.filterRelevantDistrict(province: selectedProvince), id: \.self) { district in
                    Text(district)
                        .tag(district)
                }
            }
            .pickerStyle(MenuPickerStyle())
            
            GreenLineTextView(title: "Address", text: $address)
            
            Spacer()
            
            NavigationLink(destination: ReasonToCapturePhotoView(),
                           isActive: $isFormFilled) {
                Button(action: {
                    
                    if registerContactDetailsViewModel.isRequiredFieldsFilled(phoneNumber: phoneNumber, email: email, province: selectedProvince, district: selectedDistrict, address: address){
                        isFormFilled = true
                        user.phoneNumber = phoneNumber
                        user.email = email
                        user.province = selectedProvince
                        user.district = selectedDistrict
                        user.address = address
                    }
                }){
                    RoundedGreenButtonText(text: "Next")
                }
                .padding(.horizontal)
                .alert(isPresented: $registerContactDetailsViewModel.showsAlert) { () -> Alert in
                    Alert(title: Text(registerContactDetailsViewModel.alertMessage))
                }
            }
        }
        .navigationBarTitle("Contact details", displayMode: .inline)
    }
}

struct RegisterContactDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterContactDetailsView()
    }
}
