//
//  RegisterContactDetailsViewModel.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-08-09.
//

import Foundation

class RegisterContactDetailsViewModel: ObservableObject {
    
    @Published var showsAlert = false
    @Published var alertMessage = ""
    
    func isRequiredFieldsFilled(phoneNumber:String, email:String, province:String, district:String, address:String) -> Bool {

        if phoneNumber.isEmpty || email.isEmpty || province.isEmpty || district.isEmpty || address.isEmpty {
            self.showsAlert = true
            self.alertMessage = "You need to fill all fields to complete the registration"
            return false
        }
        else if !email.isValidEmail {
            self.showsAlert = true
            self.alertMessage = "Please enter a valid eMail address"
            return false
        }
        
        return true
    }
    
    func filterRelevantDistrict(province: String) -> [String] {
        
        var filteredDistricts: [String]
        
        switch province {
        case "Central":
            filteredDistricts = ["Matale", "Kandy", "Nuwara Eliya"]
        case "Eastern":
            filteredDistricts = ["Trincomalee", "Batticaloa", "Batticaloa"]
        case "North Central":
            filteredDistricts = ["Anuradhapura", "Polonnaruwa"]
        case "North Western":
            filteredDistricts = ["Puttalam", "Kurunegala"]
        case "Northern":
            filteredDistricts = ["Jaffna", "Kilinochchi", "Mannar", "Mullaitivu", "Vavuniya"]
        case "Sabaragamuwa":
            filteredDistricts = ["Kegalle", "Ratnapura"]
        case "Southern":
            filteredDistricts = ["Hambantota", "Matara", "Galle"]
        case "Uva":
            filteredDistricts = ["Badulla", "Monaragala"]
        case "Western":
            filteredDistricts = ["Gampaha", "Colombo", "Kalutara"]
        default:
            filteredDistricts = [""]
        }
        
        return filteredDistricts
    }
}
