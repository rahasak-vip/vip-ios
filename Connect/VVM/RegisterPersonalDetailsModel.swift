//
//  RegisterPersonalDetailsModel.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-08-07.
//

import Foundation

class RegisterPersonalDetailsModel: ObservableObject {
    
    @Published var showsAlert = false
    @Published var alertMessage = ""
    
    func isRequiredFieldsFilled(nic:String, name:String, gender:String, age:String, dob:Date) -> Bool {

        if nic.isEmpty || name.isEmpty || gender.isEmpty || age.isEmpty {
            self.showsAlert = true
            self.alertMessage = "You need to fill all fields to complete the registration"
            return false
        }
        else if isBelowTheMinimumAge(dob: dob) {
            self.showsAlert = true
            self.alertMessage = "You have to be above 18 years old to apply"
            return false
        }
        else if !isValidNIC(paramNic: nic) {
            self.showsAlert = true
            self.alertMessage = "Please provide a valid NIC"
            return false
        }
        
        return true
    }
    
    func isBelowTheMinimumAge(dob:Date) -> Bool {
        
        let fmt = ISO8601DateFormatter()
        
        let date1 = fmt.date(from: fmt.string(from: dob))!
        let date2 = fmt.date(from: fmt.string(from: Date()))!

        let diffs = Calendar.current.dateComponents([.year], from: date1, to: date2)
        
        if diffs.year ?? 0 < 18 {
            return true
        }
        
        return false
    }
    
    func dobToString(dob:Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.apiDateFormat
        return dateFormatter.string(from: dob)
    }
    
    func isValidNIC(paramNic: String) -> Bool {
        
        let nicPattern = #"^([0-9]{9}[x|X|v|V]|[0-9]{12})$"#
        
        let result = paramNic.range(
            of: nicPattern,
            options: .regularExpression
        )
        
        return (result != nil)
    }
}
