//
//  RegisterPersonalDetailsView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 7/22/21.
//

import SwiftUI

struct RegisterPersonalDetailsView: View {
    
    @State var nic: String = ""
    @State var name: String = ""
    @State private var selectedGender = ""
    @State var age: String = ""
    @State var dob = Date()
    @Environment(\.colorScheme) var colorScheme
    @StateObject var registerPersonalDetailsModel = RegisterPersonalDetailsModel()
    @State var isFormFilled = false
    @EnvironmentObject var user: User
    
    var body: some View {
        VStack {
            Spacer()
            GreenLineTextView(title: "NIC", text: $nic, autoCapType: .allCharacters)
            GreenLineTextView(title: "Name", text: $name, autoCapType: .words)
                .textContentType(.name)
            
            Picker(selection: $selectedGender,
                   label: GreenLineTextView(title: "Gender", text: $selectedGender)
            ) {
                ForEach(Constants.genderArray, id: \.self) { gender in
                    Text(gender)
                        .tag(gender)
                }
            }
            .pickerStyle(MenuPickerStyle())
            
            GreenLineTextView(title: "Age", text: $age, keyboardType: .numberPad)
            
            DatePicker("DOB", selection: $dob, displayedComponents: .date)
                .datePickerStyle(CompactDatePickerStyle())
                .padding(.all, 5)
                .background(colorScheme == .dark ? Color.black : Color.white)
                .overlay(
                    Rectangle()
                        .frame(height: 1)
                        .foregroundColor(Color.green.opacity(0.2)),
                    alignment: .bottom
                )
                .padding([.leading, .trailing], 40)
                .padding([.bottom], 20)
            
            Spacer()
            
            NavigationLink(destination: RegisterContactDetailsView(),
                           isActive: $isFormFilled) {
                Button(action: {
                    
                    if registerPersonalDetailsModel.isRequiredFieldsFilled(nic: nic, name: name, gender: selectedGender, age: age, dob: dob) {
                        isFormFilled = true
                        user.nic = nic
                        user.name = name
                        user.gender = selectedGender
                        user.age = Int(age) ?? 0
                        user.dob = registerPersonalDetailsModel.dobToString(dob: dob)
                    }
                }){
                    RoundedGreenButtonText(text: "Next")
                }
                .padding(.horizontal)
                .alert(isPresented: $registerPersonalDetailsModel.showsAlert) { () -> Alert in
                    Alert(title: Text(registerPersonalDetailsModel.alertMessage))
                }
            }
        }
        .navigationBarTitle("Personal details", displayMode: .inline)
    }
}

struct RegisterPersonalDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterPersonalDetailsView()
    }
}
