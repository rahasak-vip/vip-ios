//
//  SelfieSignView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 7/30/21.
//

import SwiftUI

struct SelfieSignView: View {
    
    @State var image: UIImage?
    @Environment(\.colorScheme) var colorScheme
    @State var isTapped = false
    @EnvironmentObject var user: User
    
    var body: some View {
        ZStack(alignment: .bottom)  {
            Image(uiImage: image!.resize(UIScreen.screenWidth, UIScreen.screenHeight)!)
                .resizable()
                .aspectRatio(contentMode: .fill)
            NavigationLink(destination: PasswordConfirmationView(),
                           isActive: $isTapped) {
                SignatureView()
                    .frame(maxWidth: .infinity, maxHeight: UIScreen.screenHeight/4)
                    .background(colorScheme == .dark ? Color.black : Color.white)
                    .opacity(/*@START_MENU_TOKEN@*/0.8/*@END_MENU_TOKEN@*/)
                    .onTapGesture {
                        isTapped = true
                    }
            }
        }
        .navigationBarTitle("Sign Selfie", displayMode: .inline)
        .onAppear() {
            user.blob = convertImageToBase64String(img: image!)
        }
    }
}

struct SelfieSignView_Previews: PreviewProvider {
    static var previews: some View {
        SelfieSignView()
    }
}

struct SignatureView: View {
    var body: some View {
        Image(systemName: "checkmark.circle").font(.largeTitle)
            .frame(width: 70, height: 70)
            .background(Color.green)
            .foregroundColor(.white)
            .clipShape(Circle())
    }
}
