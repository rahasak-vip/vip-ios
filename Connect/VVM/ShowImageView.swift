//
//  ShowImageView.swift
//  LKGymTrainers
//
//  Created by Arosha Piyadigama on 2021-09-01.
//

import SwiftUI

struct ShowImageView: View {
    
    var blobStrValue:String = ""
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.colorScheme) var colorScheme
    
    var body: some View {
        ZStack {
            
            if !blobStrValue.isEmpty {
                Image(uiImage: blobStrValue.toImage()!)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .ignoresSafeArea()
            }
            else {
                Image(systemName: "photo")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .ignoresSafeArea()
            }
        }
        .background(colorScheme == .dark ? Color.black : Color.white)
        .onTapGesture {
            presentationMode.wrappedValue.dismiss()
        }
    }
}

struct ShowImageView_Previews: PreviewProvider {
    static var previews: some View {
        ShowImageView()
    }
}
