//
//  TermsAndConditionsView.swift
//  Connect
//
//  Created by Arosha Piyadigama on 2021-07-31.
//

import SwiftUI
import PDFKit

struct TermsAndConditionsView: View {
    
    let fileUrl = Bundle.main.url(forResource: "TermsAndConditionsISM", withExtension: "pdf")!
    
    var body: some View {
        
        PDFKitRepresentedView(fileUrl)
            .ignoresSafeArea()
            .navigationBarTitle("Terms of use", displayMode: .inline)
    }
}

struct TermsAndConditionsView_Previews: PreviewProvider {
    static var previews: some View {
        TermsAndConditionsView()
    }
}
