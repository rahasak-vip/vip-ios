v-1.0 b-7 :

    UI:
    - Contact Details view
    - Terms & conditions added as a PDF document
    Features:
    - Ability to run on iOS 15
    - Digital signature for qualifications register and retrieve

v-1.0 b-6 :

    UI:
    - Identity view design updated
    - Preference view design updated
    Features:
    - GeosansLight.ttf font style added and update only PreferenceView
    - Camera permission check logic added

v-1.0 b-5 :

    Bug fixes:
    - Reduced the upload image quality
    UI:
    - Professional qualifications UI updated
    Features:
    - App name changed
    - App icons changes

v-1.0 b-4 : 

    All the API included TestFlight version 
